#include "DNNObjectDetector.h"

#include <opencv2/highgui.hpp>
#include "utils.cpp"

const float nmsThreshold = 0.4;
const float scoreThreshold = 0.5;

/**
 * Create a DNNObjectDetector instance.
 *
 * @param net the DNN network model
 * @param netObjectClasses list of supported object classes of the passed DNN model.
 * @return
 */
DNNObjectDetector DNNObjectDetector::CreateDNNObjectDetector(const Net &net, vector<string> netObjectClasses) {
    DNNObjectDetector::net = net;
    DNNObjectDetector::netObjectClasses = netObjectClasses;
    return {};
}

/**
 * Identify the object on the image by the current DNN network.
 *
 * The detected object will be classified by the actual DNN model.
 */
void DNNObjectDetector::detectObjectOnImage(Mat &image) {
    vector<Mat> predictionOfClassesOfTheDetectedObject;

    net.setInput(
    blobFromImage(
        image,1/255.0,Size(DNNObjectDetector::dnnInputW, DNNObjectDetector::dnnInputH),Scalar(0,0,0),true,false
        )
    );
    // Forwarding the actual network layer to get the identified object class at the end of the DNN model.
    net.forward(predictionOfClassesOfTheDetectedObject, forwardingNetLayers(net));

    drawPrediction(image, predictionOfClassesOfTheDetectedObject);
}

/**
 * Forwarding the actual DNN layer(s) get be able to recognize the class of the detected object.
 * @param dnnModel
 * @return
 */
vector<String> DNNObjectDetector::forwardingNetLayers(Net &dnnModel) {
    static vector<String> dnnLayers;

    if (dnnLayers.empty())
    {
        //Get the indices of the output layers, i.e. the layers with unconnected outputs
        vector<int> outLayers = dnnModel.getUnconnectedOutLayers();

        //get the dnnLayers of all the layers in the network
        vector<String> layersNames = dnnModel.getLayerNames();

        // Get the dnnLayers of the output layers in dnnLayers
        dnnLayers.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
            dnnLayers[i] = layersNames[outLayers[i] - 1];
    }

    return dnnLayers;
}

/**
 * Draw Prediction
 * @param frame image
 * @param outs network prediction results
 */
void DNNObjectDetector::drawPrediction(Mat &frame, const vector<Mat> &outs) {
    vector<int> classIds;
    vector<float> confidences;
    vector<Rect> boxes;

    for (const auto & out : outs)
    {
        // Scan through all the bounding boxes output from the network and keep only the
        // ones with high confidence scores. Assign the box's class label as the class
        // with the highest score for the box.
        float* data = (float*)out.data;
        for (int j = 0; j < out.rows; ++j, data += out.cols)
        {
            Mat scores = out.row(j).colRange(5, out.cols);
            Point classIdPoint;
            double confidence;
            // Get the value and location of the maximum score
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > scoreThreshold)
            {
                int centerX = (int)(data[0] * frame.cols);
                int centerY = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
                int left = centerX - width / 2;
                int top = centerY - height / 2;

                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.emplace_back(left, top, width, height);
            }
        }
    }

    // Perform non-maximum suppression to eliminate redundant overlapping boxes with lower confidences
    vector<int> indices;
    NMSBoxes(boxes, confidences, scoreThreshold, nmsThreshold, indices);
    for (int idx : indices)
    {
        Rect box = boxes[idx];
        drawPredictionOnImage(netObjectClasses, classIds[idx], confidences[idx], box.x, box.y, box.x + box.width, box.y + box.height, frame);
    }
    showPrediction(frame);
}

/**
 * Showing the object detection result on an image.
 *
 * @param frame image
 */
void DNNObjectDetector::showPrediction(const Mat &frame) const {// Write the frame with the detection boxes
    Mat detectedFrame;
    frame.convertTo(detectedFrame, CV_8U);
    imshow("Image [Detected]", detectedFrame);
}
