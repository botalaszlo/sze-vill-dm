#include "TrackingObject.h"
#include "utils.cpp"

/**
 * Constructor
 *
 * @param grayScaledImage
 * @param currentFrame
 * @param classifications
 */
TrackingObject::TrackingObject(const Mat &grayScaledImage, int currentFrame, const string &resultImageSavingPath) {
    this->image = grayScaledImage;
    this->currentFrame = currentFrame;
    this->savingPath = resultImageSavingPath;
}

/**
 * @param contours on the current frame
 * @return the found largest contour
 */
vector<Point> TrackingObject::findLargestContour(vector<vector<Point>> contours) {
    unsigned int maxSizeOfContour = 0;
    vector<Point> largestContour = contours[0];
    for (auto & contour : contours) {
        unsigned long contourSize = contour.size();

        if (contourSize > maxSizeOfContour) {
            maxSizeOfContour = contourSize;
            largestContour = contour;
        }
    }

    return largestContour;
}

/**
 * Detecting the object on the actual image
 */
bool TrackingObject::detectTargets() {
    vector<Vec4i> hierarchy;
    vector<vector<Point>> contours;

    threshold(image, image, 128, 255, THRESH_BINARY);
    findContours(image, contours, hierarchy, RETR_LIST, CHAIN_APPROX_NONE);

    imageContour = findLargestContour(contours);

    createDetectedTargets();
    trackDetectedTargets();
    saveDetectedTargets();

    return isTargetDetected;
}

void TrackingObject::createDetectedTargets() {
    vector<Point> contouredPoly;

    // Calculating the bounded rectangle by polygonal curves
    approxPolyDP(imageContour, contouredPoly, 3, false);
    // Bonding the rectangle of the polygonal curves
    Rect boundRectOfObject = boundingRect(contouredPoly);

    /*
    cout << "Bounding Rect:" << endl;
    cout << "X:" << boundRectOfObject.x << endl;
    cout << "Y:" << boundRectOfObject.y << endl;
    cout << "Width:" << boundRectOfObject.width << endl;
    cout << "Height:" << boundRectOfObject.height << endl;
    */

    // check if object is detected -- avoiding the full image contour
    if (isLowerSizeThanImageSize(boundRectOfObject)) {
        TargetObject objectTarget(
            boundRectOfObject.x,
            boundRectOfObject.y,
            boundRectOfObject.width,
            boundRectOfObject.height,
            currentFrame
        );
        detectedTargets.push_back(objectTarget);

        isTargetDetected = true;
    } else {
        isTargetDetected = false;
    }
    /*
    cout << "Numbers of detected Object:" << detectedTargets.size() << endl;
    */
}

/**
 * Avoiding the false alarm on the noise.
 * @param boundRectOfObject the rectangle of the contour
 * @return is the rectangle size lower than the current image size.
 */
bool TrackingObject::isLowerSizeThanImageSize(const Rect &boundRectOfObject) const {
    return boundRectOfObject.width > 0 && boundRectOfObject.width < image.size().width;
}

/**
 * Tracking the detected targets.
 * Using the Kalman-filter for the tracking.
 */
void TrackingObject::trackDetectedTargets() {
    if (trackedTargets.empty()) {
        for (const TargetObject& detectedObject : detectedTargets) {
            trackedTargets.emplace_back(detectedObject);
        }
    }

    if(!trackedTargets.empty())
    {
        for (const TargetObject& detectedTarget: detectedTargets) {
            int trackFlag = 0;

            for (auto & trackedTarget : trackedTargets) {

                if (trackedTarget.isSameObject(detectedTarget))
                {
                    trackedTarget.SetCoordinateInfo(detectedTarget.X, detectedTarget.Y, detectedTarget.Width, detectedTarget.Height);

                    trackedTarget.kalmanFilter->correct(
                        getMatForMeasurement(trackedTarget)
                    );
                    trackedTarget.kalmanFilter->predict();
                    trackedTarget.LastFrame = currentFrame;
                }
                trackFlag = 1;
            }

            if (trackFlag == 0)
            {
                trackedTargets.emplace_back(detectedTarget);
            }
        }
    }
}

/**
 * Save the detected targets into image file
 */
void TrackingObject::saveDetectedTargets() {
    auto item = trackedTargets.begin();
    while(item != trackedTargets.end())
    {
        Mat resultImg = image;
        cvtColor(image, resultImg, COLOR_GRAY2RGB);
        rectangle(resultImg, getRectFromTargetObject(item), redColour(), 1);

        imshow("Image Sequence [FOR TRACKING]", resultImg);
        imwrite(savingPath + to_string(currentFrame) + ".jpg", resultImg);

        item++;
    }
}
