#ifndef DM2_TRACKINGOBJECT_H
#define DM2_TRACKINGOBJECT_H

#include "opencv4/opencv2/opencv.hpp"
#include "TargetObject.h"
#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

class TrackingObject {
    private:
        Mat image;
        vector<Point> imageContour;
        vector<TargetObject> detectedTargets;
        vector<TargetObject> trackedTargets;
        int currentFrame;
        bool isTargetDetected;
        // Saving path for the result image files.
        string savingPath;
    public:
        TrackingObject(const Mat& grayScaledImage, int currentFrame, const string& resultImageSavingPath);
        bool detectTargets();
    private:
        static vector<Point> findLargestContour(vector<vector<Point>> contours);
        void createDetectedTargets();
        void trackDetectedTargets();
        void saveDetectedTargets();
        bool isLowerSizeThanImageSize(const Rect &boundRectOfObject) const;
};


#endif //DM2_TRACKINGOBJECT_H
