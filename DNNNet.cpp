#include <fstream>
#include "DNNNet.h"

/**
 * Create a DNN Network based on the passed model configuration.
 * @param modelWeights DNN related weights file
 * @param modelConfig  DNN config file
 * @param classifications available classifications of the actual DNN
 *
 * @return the dnn network
 */
Net DNNNet::createDNNNet(const string &modelWeights, const string &modelConfig, const string &classifications) {
    Net net = readNet(modelWeights, modelConfig);

    net.setPreferableBackend(DNN_BACKEND_OPENCV);
    net.setPreferableTarget(DNN_TARGET_CPU);

    return net;
}

/**
 * Loading the object classes (aka classifications) from the file.
 *
 * @param classifications path to the file
 *
 * @return the object classes.
 */
vector<string> DNNNet::loadObjectClasses(const string& classifications) {
    vector<string> objectClasses;

    ifstream ifs(classifications.c_str());

    string line;
    while (getline(ifs, line))
        objectClasses.push_back(line);

    return objectClasses;
}
