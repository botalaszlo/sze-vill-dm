#include "opencv2/opencv.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#include <cmath>
#include <fstream>

using namespace cv;
using namespace std;

#ifndef DM1_TargetObject_H
#define DM1_TargetObject_H

class TargetObject {
    public:
        int X;
        int Y;
        int Width;
        int Height;
        int LastFrame;
        // Are curr and next objects same
        bool isSameObject(const TargetObject&) const;
        // Get the central point of the object
        Point centralPoint() const {
            return {(X + Width / 2), (Y + Height / 2)};
        };

    public:
        // Constructors
        TargetObject();
        TargetObject(const TargetObject& objectTarget);
        TargetObject(int x, int y, int w, int h, int f);
        // Kalman Filter
        KalmanFilter* kalmanFilter{};
        // Setters
        void SetCoordinateInfo(int x, int y, int w, int h);

    private:
        void initKalmanFilter();
};

#endif //DM1_TargetObject_H

