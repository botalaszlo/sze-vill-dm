#ifndef DM2_DNNOBJECTDETECTOR_H
#define DM2_DNNOBJECTDETECTOR_H


#include "DNNNet.h"

using namespace cv;

class DNNObjectDetector {
    private:
        static const unsigned int dnnInputW = 416;
        static const unsigned int dnnInputH = 416;
        inline static Net net;
        inline static vector<string> netObjectClasses;
    public:
        //explicit DNNObjectDetector(const Net& net, vector<string> netObjectClasses);
        static DNNObjectDetector CreateDNNObjectDetector(const Net& net, vector<string> netObjectClasses);
        void detectObjectOnImage(Mat &image);
    private:
        vector<String> forwardingNetLayers(Net &dnnModel);
        void drawPrediction(Mat &frame, const vector<Mat> &outs);

    void showPrediction(const Mat &frame) const;
};


#endif //DM2_DNNOBJECTDETECTOR_H
