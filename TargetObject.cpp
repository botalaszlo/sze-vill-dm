#include "TargetObject.h"

int const AREA_MULTIPLIER = 3;

TargetObject::TargetObject() {
    X = -1;
    Y = -1;
    Width = -1;
    Height = -1;
    LastFrame = -1;

    initKalmanFilter();
}

void TargetObject::initKalmanFilter() {
    kalmanFilter = new KalmanFilter(4, 2, 0);

    Mat state(4, 1, CV_32F);
    Mat processNoise(4, 1, CV_32F);
    Mat measurement = Mat::zeros(2, 1, CV_32F);

    randn(state, Scalar::all(0), Scalar::all(0.1));

    kalmanFilter->transitionMatrix = (
        Mat_<float>(4, 4) <<
        1, 0, 1, 0,
        0, 1, 0, 1,
        0, 0, 1, 0,
        0, 0, 0, 1
    );

    setIdentity(kalmanFilter->measurementMatrix);
    setIdentity(kalmanFilter->processNoiseCov, Scalar::all(1e-6));
    setIdentity(kalmanFilter->measurementNoiseCov, Scalar::all(1e-2));
    setIdentity(kalmanFilter->errorCovPost, Scalar::all(1));

    randn(kalmanFilter->statePost, Scalar::all(0), Scalar::all(0.1));
}

TargetObject::TargetObject(const TargetObject& objectTarget) {
    X = objectTarget.X;
    Y = objectTarget.Y;
    Width = objectTarget.Width;
    Height = objectTarget.Height;
    LastFrame = objectTarget.LastFrame;
    kalmanFilter = objectTarget.kalmanFilter;
}

TargetObject::TargetObject(int x, int y, int w, int h, int f) {
    X = x;
    Y = y;
    Width = w;
    Height = h;
    LastFrame = f;

    initKalmanFilter();
}

bool TargetObject::isSameObject(const TargetObject& newTargetObject) const {
    int thisObjectArea = Width * Height;
    int thatObjectArea = newTargetObject.Width * newTargetObject.Height;

    bool hasAreaAnomaly = AREA_MULTIPLIER * thisObjectArea < thatObjectArea || thisObjectArea > AREA_MULTIPLIER * thatObjectArea;

    if (hasAreaAnomaly)
        return false;

    Mat prediction = kalmanFilter->predict();

    Point predictCenter(prediction.at<float>(0.0), prediction.at<float>(1.0));

    if (predictCenter.x > newTargetObject.X
        && predictCenter.x < (newTargetObject.X + newTargetObject.Width)
        && predictCenter.y > newTargetObject.Y
        && predictCenter.y < (newTargetObject.Y + newTargetObject.Height))
    {
        return true;
    }

    if (newTargetObject.centralPoint().x > X
         && newTargetObject.centralPoint().x < (X + Width)
         && newTargetObject.centralPoint().y > Y
         && newTargetObject.centralPoint().y < (Y + Height))
    {
        return true;
    }

    return false;
}

void TargetObject::SetCoordinateInfo(int x, int y, int w, int h) {
    X = x;
    Y = y;
    Width = w;
    Height = h;
}




