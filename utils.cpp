#include <opencv4/opencv2/core/core.hpp>
#include <vector>

#include "TargetObject.h"

using namespace std;
using namespace cv;

static Mat OtsuThresholdingImage(const Mat &imgGray, double &actualThreshold) {
    Mat imgOtsu;
    double thresholdValue = threshold(imgGray, imgOtsu, 0, 255, THRESH_BINARY | THRESH_OTSU);
    actualThreshold = thresholdValue;

    return imgOtsu;
}

static Scalar redColour() {
   return { 0, 0, 200};
}

static Rect getRectFromTargetObject(const vector<TargetObject>::iterator &item) {
    Rect rect;
    rect.x = item->X;
    rect.y = item->Y;
    rect.width = item->Width;
    rect.height = item->Height;
    return rect;
}

static Mat getMatForMeasurement(const TargetObject &trackedTarget) {
    Mat measurement = Mat::zeros(2, 1, CV_32FC1);
    measurement.at<float>(0) = float(trackedTarget.X + trackedTarget.Width / 2.0 - 1);
    measurement.at<float>(1) = float(trackedTarget.Y + trackedTarget.Height / 2.0 - 1);
    return measurement;
}

static void drawPredictionOnImage(vector<string> objectClassifications, int &classificationId, float &confidence, int left, int top, int right, int bottom, const Mat &frame) {
    cout << "drawPredictionOnImage" << endl;

    //Draw a rectangle displaying the bounding box
    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0, 0, 255));
    //Get the label for the class name and its confidence
    string label = format("%.2f", confidence);

    if (!objectClassifications.empty())
    {
        CV_Assert(classificationId < (int)objectClassifications.size());
        label = objectClassifications[classificationId] + ":" + label;
        cout << "Object Classification result: " << objectClassifications[classificationId] << "| Confidence:" << confidence << endl;
    }

    //Display the label at the top of the bounding box
    int baseLine;
    Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
    putText(frame, label, Point(left, max(top, labelSize.height)), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255,255,255));
}
