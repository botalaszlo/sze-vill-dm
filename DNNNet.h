#ifndef DM2_DNNNET_H
#define DM2_DNNNET_H

#include <string>
#include <opencv2/dnn/dnn.hpp>

using namespace std;
using namespace cv::dnn;

class DNNNet {
    public:
        static Net createDNNNet(const string& modelWeights, const string& modelConfig, const string& classNames);
        static vector<string> loadObjectClasses(const string& basicString);
};


#endif //DM2_DNNNET_H
