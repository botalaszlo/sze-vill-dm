#include <iostream>

#include "opencv4/opencv2/opencv.hpp"
#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>

#include "TargetObject.h"
#include "utils.cpp"
#include "TrackingObject.h"
#include "DNNNet.h"
#include "DNNObjectDetector.h"

using namespace cv;
using namespace std;
using namespace cv::dnn;

#define PROJECT_DIR "/home/botalasz/Projects/dm2"
#define FILE_SEQUENCE_NAME PROJECT_DIR "/img01/%04d.jpg"
#define FILE_SEQUENCE_SAVE_BOX PROJECT_DIR "/result/box"
#define DNN_MODEL PROJECT_DIR  "/dnn/yolov3.weights"
#define DNN_CONFIG PROJECT_DIR "/dnn/yolov3.cfg"
#define DNN_NAMES PROJECT_DIR "/dnn/coco.names"

/**
 * Variable definitions
 */
Mat image;
Mat imageMask;

int currFrame = 0;
int initialThreshold = 100;
double actualThreshold = 0.0;
bool isTargetDetected = false;

/**
 * Function definitions
 */
void processTrackbar(int, void*);

int main() {

    #if (!defined(PROJECT_DIR))
        cerr << "PROJECT_DIR is not defined or Empty!" << endl;
    #endif

    // Loading Images
    VideoCapture sequence(FILE_SEQUENCE_NAME);
    // Error check on the video sequence
    if (!sequence.isOpened()) {
        cerr << "Failed to open image sequence" << endl;
        return -1;
    }

    // DNN Network
    Net net = DNNNet::createDNNNet(DNN_MODEL, DNN_CONFIG, DNN_NAMES);
    DNNObjectDetector dnnObjectDetector = DNNObjectDetector::CreateDNNObjectDetector(net, DNNNet::loadObjectClasses(DNN_NAMES));
    // Making background subtraction
    Ptr<BackgroundSubtractor> pBackSub = createBackgroundSubtractorKNN();

    for(;;)
    {
        sequence >> image;
        if(image.empty())
        {
            cout << "End of Sequence" << endl;
            break;
        }

        // Background subtraction
        pBackSub->apply(image, imageMask);
        /*
        imshow("Image [ORIGINAL]", image);
        imshow("Image [FOREGROUND MASK]", imageMask);
        */

        // Tracking the Otsu-based grayscale image
        createTrackbar("Otsu initialThreshold", "Track Win Name", &initialThreshold, 255, processTrackbar);
        processTrackbar(0, 0);

        // DNN classifies the detected object
        if (isTargetDetected) {
            dnnObjectDetector.detectObjectOnImage(image);
        }

        // Frame counting
        currFrame++;

        // Delayed waiting for the exit buttons
        char key = (char)waitKey(100);
        if(key == 'q' || key == 'Q' || key == 27)
            break;
    }

    //waitKey(1);
    return 0;
}

void processTrackbar(int, void*) {
    Mat otsuImage = OtsuThresholdingImage(imageMask, actualThreshold);
    TrackingObject objectDetector(otsuImage, currFrame, FILE_SEQUENCE_SAVE_BOX);
    isTargetDetected = objectDetector.detectTargets();
}
